﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeTransform : MonoBehaviour
{

    public RectTransform videopanel;
    public Vector3 orirect;
    // Start is called before the first frame update
    void Start()
    {
        orirect = videopanel.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(orirect);
    }

    public void ChangeSize()
    {

        videopanel.localPosition = new Vector3(1f, -2.9453f, -294.08f);

        videopanel.localScale = new Vector3(0.4701078f, 0.4701078f, 0.4701078f);

    }

    public void ChangeSizeBack()
    {

        //  videopanel.localPosition = new Vector3(13.78f, -61.45f, -286f);
        videopanel.localPosition = orirect;

        videopanel.localScale = new Vector3(0.3f, 0.3f, 0.3f);

    }

}
