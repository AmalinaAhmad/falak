﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpashScreen : MonoBehaviour
{

    public Animator Black;
    public bool SceneSplash;
    public int scene;
    // Start is called before the first frame update
    void Start()
    {
        if (SceneSplash)
        {
            Invoke("EndAnimation", 10f);

        }


        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EndAnimation() {

        Black.SetTrigger("FadeOut");
        SceneManager.LoadScene(scene);

    }
    public void EndAnimation1(int scene1)
    {

        Black.SetTrigger("FadeOut");
        SceneManager.LoadScene(scene1);

    }
}

