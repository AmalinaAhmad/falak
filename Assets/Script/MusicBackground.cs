﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicBackground : MonoBehaviour
{


    public Canvas cv;
    
    // Start is called before the first frame update
    void Start()
    {

        DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update()
    {
        cv.worldCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }
}
