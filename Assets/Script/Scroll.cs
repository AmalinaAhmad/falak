﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scroll : MonoBehaviour
{
    public Scrollbar Target;
    public Button TheOtherButton;
    public float Value = 1.0f;


    public void Increment()
    {
        Target.value = Target.value + Value;
    }

    public void Decrement()
    {
        Target.value = Target.value - Value;


    }



}
