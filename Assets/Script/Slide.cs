﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Slide : MonoBehaviour
{

    public int ama = 0;
    public int ValueMax;

    public GameObject[] SlideShow;
    public Button Next, Previous;
    // Start is called before the first frame update
    void Start()
    {
        Previous.interactable = false;

    }

    // Update is called once per frame
    void Update()
    {

        if (ama >= 1)
        {

            Previous.interactable = true;


        }
    }

    public void Increment()
    {

        ama++;

        for (int i = 0; i <SlideShow.Length; i++)
        {


            SlideShow[i].SetActive(false);
        }

        if (ama == ValueMax)
        {
            Next.interactable = false;
            SlideShow[ama].SetActive(true);
        }

        SlideShow[ama].SetActive(true);
        Previous.interactable = true;


    }


    public void Decrement()
    {

        ama--;

        for (int i = 0; i < SlideShow.Length; i++)
        {


            SlideShow[i].SetActive(false);
        }

        if (ama == 0)
        {
            Previous.interactable = false;
            SlideShow[ama].SetActive(true);
        }

        SlideShow[ama].SetActive(true);
        Next.interactable = true;






    }


    public void ChangeRect(GameObject Image1)
    {
        Image1.SetActive(true);

    }

    public void Close(GameObject Image1)
    {

        Image1.SetActive(false);


    }
}